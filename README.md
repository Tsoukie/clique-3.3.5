# Clique (3.3.5a - Private Server)
**`Simply powerful click-casting interface, with support for CompactRaidFrame.`**

---

### ⚠ Note: This version is not related or affiliated with the official retail/classic addon!
### 📥 [Installation](#-installation-1)
### 📋 [Report Issue](https://gitlab.com/Tsoukie/clique-3.3.5/-/issues)
### 💬 [FAQ](#-faq-1)
### ❤️ [Support & Credit](#%EF%B8%8F-support-credit-1)

---
### Features:
- **CompactRaidFrame:** _Added support for [CompactRaidFrame](https://gitlab.com/Tsoukie/compactraidframe-3.3.5)_

<!-- blank line -->
<br>
<!-- blank line -->

# 📥 Installation

1. Download Latest Release `[.zip, .gz, ...]`:
	- <a href="https://gitlab.com/Tsoukie/clique-3.3.5/-/releases/permalink/latest" target="_blank">`📥 Clique-3.3.5`</a>
2. Extract **both** the downloaded compressed files _(eg. Right-Click -> Extract-All)_.
3. Navigate within each extracted folder(s) looking for the following: `Clique`.
4. Move folder(s) named `Clique` to your `Interface\AddOns\` folder.
5. Re-launch game.

<!-- blank line -->
<br>
<!-- blank line -->


# 💬 FAQ

> I found a bug!

Please 📋 [report the issue](https://gitlab.com/Tsoukie/clique-3.3.5/-/issues) with as much detail as possible.

<!-- blank line -->
<br>
<!-- blank line -->


# ❤️ Support & Credit
 
If you wish to show some support you can do so [here](https://streamlabs.com/tsoukielol/tip). Tips are completely voluntary and aren't required to download my projects, however, they are _very_ much appreciated. They allow me to devote more time to creating things I truly enjoy. 💜

<!-- blank line -->
<br>
<!-- blank line -->
  
_This_ version is modified and maintained by [Tsoukie](https://gitlab.com/Tsoukie/) and is **not** related or affiliated with any other version.