## Interface: 30300 
## Title: Clique
## Author: Tsoukie
## Version: 1.0.1
## Notes: Simply powerful click-casting interface
## SavedVariables: CliqueDB
## OptionalDeps: Dongle

Dongle.lua
Clique.xml
Clique.lua
Localization.enUS.lua
Localization.esES.lua
Localization.esMX.lua
Localization.frFR.lua
Localization.deDE.lua
Localization.koKR.lua
Localization.ruRU.lua
Localization.zhCN.lua
Localization.zhTW.lua
CliqueOptions.lua
CliqueUtils.lua
